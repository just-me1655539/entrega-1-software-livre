ALL = programa

all: $(ALL) 
	
programa: main.o function1.o function2.o function3.o
	gcc $^ -o programa
	
%.o: %.c
	gcc -c $<
	
clean:
	rm -f *.o
	
